# homework-01 Mauricio Mage #

## Acerda del Proyecto ##
Este proyecto contempla todos los puntos resueltos que se pide en el Homework 01 de la Materia Enrono Web.
Se creo en BitBucket el workspace docker-exercises, dentro del mismo se creo el proyecto hw-01 y dentro el repositorio llamado tambien hw-01.
 
A continuacion se aclararan cada uno de los ejercicios consecuentes para acceder a una mejor lectura de como se resolvieron los mismos. 

### Punto 1) ###
Se encuentra redacta la respuesta en el fichero: answer-exercise-01.md

### Punto 2) ###
Se encuentra redacta la respuesta en el fichero: answer-exercise-02.md

### Punto 3) ###
Como se indica en el enunciado del homework, se creo una carpeta para adjuntar documentos necesarios de como se llego al resultado de dicho punto.
Si ingresa a la carpeta exercise-03 va a poder descargar y abrir (si no tiene Office puede subirlo a Drive y abrirlo con google Docs) el archivo para
visualizar cada uno de los paso que se siguio para obtener el resultado esperado en base a la imagen de NGINX.

### Punto 4) ###
Este punto incluye el Dockerfile de este proyecto, y a su vez tambien hay una carpeta llamada exercise-04 donde contiene un archivo con todos los paso que se siguieron para obtener el resultado deseado en base a HEALTHCHECK

### Punto 5) ###
Este punto incluye el fichero docker-compose.yml de este proyecto, y a su vez tambien hay una carpeta llamada exercise-05 donde contiene un archivo con todos los paso que se siguieron para obtener el resultado deseado en base a ELASTIC SEARCH Y KIBANA.


