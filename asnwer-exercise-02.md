## Diferencia entre el uso de la instrucción ADD y COPY ##

### COPY ###

La sentencia COPY  en Dockerfile, toma un Origen y Destino. Solo permite copiar en un archivo o directorio local desde el host en la propia imagen de Docker.

### ADD ###

ADD también te permite hacer eso, pero también es compatible con otras 2 fuentes. Primero, se puede usar una URL en lugar de un archivo/directorio local. En segundo lugar, puede extraer un archivo tar desde el origen directamente al destino.
