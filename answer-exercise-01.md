## Diferencia entre el uso de la instrucción CMD y ENTRYPOINT en Dockerfile: ##

### CMD ###

Define comandos y/o parámetros predeterminados para un contenedor. CMD es una instrucción que es mejor usar si se necesita un comando predeterminado. Si un Dockerfile tiene varias CMD, solo aplica las instrucciones de la última.

### ENTRYPOINT ###

En cambio es preferible usar cuando se necesita definir un contenedor con un ejecutable específico. 
