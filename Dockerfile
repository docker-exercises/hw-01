# node image v 10.16.0
FROM node:10.16.0

# create the app folder
WORKDIR /usr/src/app

# copy the package.json file to the app fodler
COPY package.json /usr/src/app/

# install dependencies
RUN npm install

# copy bundle files to the app folder
COPY . /usr/src/app

# Check health each 45 seconds with a timeout of 5 seconds and with a start period = 10 seconds. 2 retries after fail.
HEALTHCHECK --interval=45s --timeout=5s --retries=2 CMD cd . || exit 1

# EXPOSE port 8080
EXPOSE 8080

# run the app
CMD ["npm", "start"]
